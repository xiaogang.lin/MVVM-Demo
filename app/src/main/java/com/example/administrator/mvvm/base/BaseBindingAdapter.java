package com.example.administrator.mvvm.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseBindingAdapter<M, B extends ViewDataBinding> extends RecyclerView.Adapter {
    private Context context;

    private ObservableArrayList<M> items = new ObservableArrayList<>();

    public BaseBindingAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        B binding = DataBindingUtil.inflate(LayoutInflater.from(context), getLayoutResId(viewType), parent, false);
        return new BaseBindingViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        B binding = DataBindingUtil.getBinding(holder.itemView);
        this.onBindItem(binding, this.items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addData(List<M> data) {
        items.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(List<M> data) {
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    protected abstract int getLayoutResId(int viewType);

    protected abstract void onBindItem(B binding, M item);

    public class BaseBindingViewHolder extends RecyclerView.ViewHolder {

        public BaseBindingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
