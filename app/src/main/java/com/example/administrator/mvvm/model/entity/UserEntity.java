package com.example.administrator.mvvm.model.entity;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.administrator.mvvm.R;

import java.io.Serializable;

/**
 * 实体类
 */
public class UserEntity{
    public String name;

    public String age;

    public String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    /**
     * BindingAdapter用来绑定自定义属性，而且方法必须是静态的
     *
     * @param imageView
     * @param url
     */
    @BindingAdapter({"image"})
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).error(R.mipmap.ic_launcher).into(imageView);
    }
}
