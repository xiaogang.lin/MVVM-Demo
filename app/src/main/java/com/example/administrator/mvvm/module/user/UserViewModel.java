package com.example.administrator.mvvm.module.user;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.administrator.mvvm.model.entity.GoodsEntity;
import com.example.administrator.mvvm.model.entity.UserEntity;
import com.example.administrator.mvvm.model.goods.GoodsRepository;
import com.example.administrator.mvvm.model.user.UserRepository;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    //LiveData是一个可被观察的数据持有者，他能感知组件的生命周期，用来关联View与ViewModel的
    private MutableLiveData<UserEntity> userLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GoodsEntity>> goodsLiveData = new MutableLiveData<>();
    //数据Model
    private UserRepository userRepository;
    private GoodsRepository goodsRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        goodsRepository = new GoodsRepository(application);
    }

    /**
     * 获取用户数据
     */
    public void getUserInfo() {
        UserEntity userEntity = userRepository.getUserInfo("1ab25inibj3882lkhanab");
        userLiveData.setValue(userEntity);
    }

    /**
     * 获取商品数据
     */
    public void getGoods() {
        List<GoodsEntity> goods = goodsRepository.getGoods();
        goodsLiveData.setValue(goods);
    }


    public MutableLiveData<UserEntity> getUserLiveData() {
        return userLiveData;
    }

    public MutableLiveData<List<GoodsEntity>> getGoodsLiveData() {
        return goodsLiveData;
    }

}
