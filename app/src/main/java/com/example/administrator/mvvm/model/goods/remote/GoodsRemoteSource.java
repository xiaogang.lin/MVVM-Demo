package com.example.administrator.mvvm.model.goods.remote;

import android.content.Context;

import com.example.administrator.mvvm.model.entity.GoodsEntity;
import com.example.administrator.mvvm.model.goods.GoodsSource;

import java.util.ArrayList;
import java.util.List;

/**
 * 远程数据源：通过服务端来存取数据，可以结合网络请求框架使用
 */
public class GoodsRemoteSource implements GoodsSource {
    Context context;

    public GoodsRemoteSource(Context context) {
        this.context = context;
    }

    @Override
    public List<GoodsEntity> getGoods() {
        //这里模拟从服务端接口获取到商品数据
        List<GoodsEntity> goods = new ArrayList<>();
        GoodsEntity g1 = new GoodsEntity();
        g1.setName("手表");
        g1.setPrice("100");
        GoodsEntity g2 = new GoodsEntity();
        g2.setName("书籍");
        g2.setPrice("51");
        GoodsEntity g3 = new GoodsEntity();
        g3.setName("钢笔");
        g3.setPrice("18");
        goods.add(g1);
        goods.add(g2);
        goods.add(g3);
        return goods;
    }
}