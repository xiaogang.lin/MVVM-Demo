package com.example.administrator.mvvm.model.user.local;

import android.content.Context;

import com.example.administrator.mvvm.model.user.UserSource;
import com.example.administrator.mvvm.model.entity.UserEntity;

/**
 * 本地数据源：可以用来存取本地缓存的数据，可以结合SharePrefence或本地数据库ORM等框架
 */
public class UserLocalSource implements UserSource {
    Context context;

    public UserLocalSource(Context context) {
        this.context = context;
    }

    @Override
    public boolean login(String userName, String password) {
        return false;
    }

    @Override
    public UserEntity getUserInfo(String token) {
        return null;
    }
}