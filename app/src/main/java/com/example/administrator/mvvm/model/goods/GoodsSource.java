package com.example.administrator.mvvm.model.goods;

import com.example.administrator.mvvm.model.entity.GoodsEntity;

import java.util.List;

public interface GoodsSource {

    List<GoodsEntity> getGoods();
}