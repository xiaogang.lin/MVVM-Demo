package com.example.administrator.mvvm.model.goods;

import android.content.Context;

import com.example.administrator.mvvm.model.entity.GoodsEntity;
import com.example.administrator.mvvm.model.goods.local.GoodsLocalSource;
import com.example.administrator.mvvm.model.goods.remote.GoodsRemoteSource;

import java.util.List;

/**
 * 用户数据仓库
 */
public class GoodsRepository implements GoodsSource {
    //本地数据源
    private final GoodsLocalSource mLocalSource;
    //远程数据源
    private final GoodsRemoteSource mRemoteSource;

    public GoodsRepository(Context context) {
        mLocalSource = new GoodsLocalSource(context);
        mRemoteSource = new GoodsRemoteSource(context);
    }

    @Override
    public List<GoodsEntity> getGoods() {
        return mRemoteSource.getGoods();
    }
}
