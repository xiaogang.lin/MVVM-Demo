package com.example.administrator.mvvm.model.goods.local;

import android.content.Context;

import com.example.administrator.mvvm.model.entity.GoodsEntity;
import com.example.administrator.mvvm.model.goods.GoodsSource;

import java.util.List;

/**
 * 本地数据源：可以用来存取本地缓存的数据，可以结合SharePrefence或本地数据库ORM等框架
 */
public class GoodsLocalSource implements GoodsSource {
    Context context;

    public GoodsLocalSource(Context context) {
        this.context = context;
    }

    @Override
    public List<GoodsEntity> getGoods() {
        return null;
    }
}