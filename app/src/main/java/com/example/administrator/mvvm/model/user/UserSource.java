package com.example.administrator.mvvm.model.user;

import com.example.administrator.mvvm.model.entity.UserEntity;

public interface UserSource {

    boolean login(String userName, String password);

    UserEntity getUserInfo(String token);
}