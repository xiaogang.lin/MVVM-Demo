package com.example.administrator.mvvm.model.user;

import android.content.Context;

import com.example.administrator.mvvm.model.entity.UserEntity;
import com.example.administrator.mvvm.model.user.local.UserLocalSource;
import com.example.administrator.mvvm.model.user.remote.UserRemoteSource;

/**
 * 用户数据仓库
 */
public class UserRepository implements UserSource {
    //本地数据源
    private final UserLocalSource mLocalSource;
    //远程数据源
    private final UserRemoteSource mRemoteSource;

    public UserRepository(Context context) {
        mLocalSource = new UserLocalSource(context);
        mRemoteSource = new UserRemoteSource(context);
    }

    @Override
    public boolean login(String userName, String password) {
        return mRemoteSource.login(userName, password);
    }

    @Override
    public UserEntity getUserInfo(String token) {
        return mRemoteSource.getUserInfo(token);
    }
}
