package com.example.administrator.mvvm.module.user;

import android.content.Context;

import com.example.administrator.mvvm.R;
import com.example.administrator.mvvm.base.BaseBindingAdapter;
import com.example.administrator.mvvm.databinding.ItemGoodsBinding;
import com.example.administrator.mvvm.model.entity.GoodsEntity;

public class GoodsAdapter extends BaseBindingAdapter<GoodsEntity, ItemGoodsBinding> {

    public GoodsAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.item_goods;
    }

    @Override
    protected void onBindItem(ItemGoodsBinding binding, GoodsEntity item) {
        binding.setGoods(item);
    }
}
