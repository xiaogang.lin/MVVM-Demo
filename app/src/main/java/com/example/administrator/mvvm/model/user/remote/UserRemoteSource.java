package com.example.administrator.mvvm.model.user.remote;

import android.content.Context;

import com.example.administrator.mvvm.model.user.UserSource;
import com.example.administrator.mvvm.model.entity.UserEntity;

/**
 * 远程数据源：通过服务端来存取数据，可以结合网络请求框架使用
 */
public class UserRemoteSource implements UserSource {
    Context context;

    public UserRemoteSource(Context context) {
        this.context = context;
    }

    @Override
    public boolean login(String userName, String passWord) {
        //这里模拟通过服务端接口完成登录
        return true;
    }

    @Override
    public UserEntity getUserInfo(String token) {
        //这里模拟通过服务端接口获取到用户数据
        UserEntity entity = new UserEntity();
        entity.setName("林晓刚");
        entity.setAge("18");
        entity.setImage("https://duoquxiang.oss-cn-shenzhen.aliyuncs.com/avatar/dqx_avatar_00255.jpg");
        return entity;
    }

}