﻿# MVVM Demo

#### 项目介绍
该Demo适合初学者来学习MVVM架构，示例简单，比较容易上手，没有引入其他第3方框架，目的是为了让
初学者先初步了解MVVM架构；该示例使用官方架构组件ViewModel + LiveData + DataBinding来架构MVVM

#### 组件说明
#### ViewModel：
主要用来处理业务逻辑并为View提供数据Model

#### LiveData：
View和ViewModel通过LiveData来传递消息和数据。LiveData是一个基于观察者模式的数据持有者，他可以
让APP中的组件观察LiveData是否发生改变，而且不需要他们之前有严格的相互依赖关系。LiveData
还会遵循应用程序组件（Activity，Fragment, Service）的生命周期状态来避免内存泄露，从而使
你的APP不会消费太多的内存（LiveData是生命周期感知的。这意味着除非fragment是激活状态（onStart
但是还没有onStop），要不然是不会发起回调的。当fragment调用onStop后， LiveData还会自动删除观察者。）。

#### DataBinding：
用来绑定View和数据Model




